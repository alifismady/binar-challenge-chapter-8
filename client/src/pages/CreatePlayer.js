import React from 'react'
import PlayerForm from '../components/PlayerForm'

const CreatePlayer = ({onSubmit}) => {
  return (
    <PlayerForm onSubmit={onSubmit}/>

  )
}

export default CreatePlayer