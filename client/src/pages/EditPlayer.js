import React from 'react'
import PlayerForm from '../components/PlayerForm'

const EditPlayer = ({onSubmit}) => {
  return (
    <PlayerForm onSubmit={onSubmit}/>
  )
}

export default EditPlayer