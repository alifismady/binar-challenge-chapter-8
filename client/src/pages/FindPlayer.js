import React from 'react'
import PlayerForm from '../components/PlayerForm'

const FindPlayer = ({onSubmit}) => {
  return (
    <PlayerForm onSubmit={onSubmit}/>
  )
}

export default FindPlayer