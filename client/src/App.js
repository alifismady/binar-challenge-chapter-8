import React from "react";
import CreatePlayer from "./pages/CreatePlayer";
import EditPlayer from "./pages/EditPlayer";
import FindPlayer from "./pages/FindPlayer";

function App() {
  const handleCreatePlayerSubmit = (formData) => {
    // Tampilkan semua input sebagai satu elemen HTML baru
    console.log(formData);
  };

  const handleEditPlayerSubmit = (formData) => {
    // Tampilkan semua input sebagai satu elemen HTML baru
    console.log(formData);
  };

  const handleSearchPlayerSubmit = (formData) => {
    // Tampilkan semua input sebagai satu elemen HTML baru
    console.log(formData);
  };

  return (
    <div>
      <h1>Create Player</h1>
      <CreatePlayer onSubmit={handleCreatePlayerSubmit} />

      <h1>Edit Player</h1>
      <EditPlayer onSubmit={handleEditPlayerSubmit} />

      <h1>Search Player</h1>
      <FindPlayer onSubmit={handleSearchPlayerSubmit} />
    </div>
  );
}

export default App;
